<%-- 
    Document   : AdminPage
    Created on : 21-Apr-2017, 11:21:21
    Author     : Dion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Admin Page</title>
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
    </head>
    <body>
        <ul>
            <li><a href="AdminPage.jsp">Home</a></li>
            <li><a href="CreateAccount.xhtml">Create Student Account</a></li>
            <li><a href="CreateInstructorAccount.xhtml">Create Instructor Account</a></li>
            <li><a href="ResetPassword.xhtml">Reset Account Password</a></li>
            <li><a href="DeleteAccount.xhtml">Delete Account</a></li>
            <li><a href="AdminTestHistory.jsp">View Test History</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <%
            if (session.getAttribute("username") == null) {
               response.sendRedirect("AdminLogin.xhtml");
            }
        %>
        <div class="login-page">
            <div class="form">
                Welcome: ${username}
            </div>
        </div>
    </body>
</html>