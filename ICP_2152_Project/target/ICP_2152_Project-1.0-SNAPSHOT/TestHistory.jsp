<%-- 
    Document   : TestHistory
    Created on : 20-Apr-2017, 19:21:51
    Author     : Anthony
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.IOException"%>
<%@page import="com.mycompany.icp_2152_project.SimpleDataSource"%>
<%@page import="com.mycompany.icp_2152_project.QueryChecker"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html>
    <style>
        div {
            width: 30%;
            height: 100%;
            text-align: center;
            margin: auto;
        }

        .tb {
            margin: auto;
        }
    </style>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
        <title>Test History</title>
    </head>
    <body>
        <ul>
            <li><a href="StudentPage.jsp">Home</a></li>
            <li><a href="TakeTestServlet">Take Test</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <div class="wrapHistory">
            <h1>Test History</h1>

            <%@page import="java.sql.DriverManager"%>
            <%@page import="java.sql.ResultSet"%>
            <%@page import="java.sql.Statement"%>
            <%@page import="java.sql.Connection"%>
            <%
                Connection conn;
                ResultSet resultSet;
                PreparedStatement preparedStmt;

                try {
                    InputStream stream = QueryChecker.class.getResourceAsStream("/database.properties");
                    SimpleDataSource.init(stream);
                } catch (IOException e) {
                    Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, e);
                }
            %>

            <table class="tb">
                <tr>
                </tr>
                <tr ID="TableHeader">
                    <td><b>Score</b></td>
                    <td><b>Completion Date</b></td>
                </tr>
                <%
                    //get the logged in users name and place it into query
                    String username = (String) session.getAttribute("username");
                    try {
                        String sql = "SELECT testNo, score, completionDate FROM results WHERE userName = ?;";
                        conn = SimpleDataSource.getConnection();
                        preparedStmt = conn.prepareStatement(sql);
                        preparedStmt.setString(1, username);
                        resultSet = preparedStmt.executeQuery();

                        while (resultSet.next()) {
                %>
                <tr ID="Results">

                    <td><%=resultSet.getString("score")%></td>
                    <td><%=resultSet.getString("completionDate")%></td>

                </tr>

                <%
                        }
                        conn.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                %>
            </table></div>
    </body>
</html>
