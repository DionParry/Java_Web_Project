<%-- 
    Document   : AddWords
    Created on : 20-Apr-2017, 21:09:10
    Author     : Anthony
--%>

<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.io.IOException"%>
<%@page import="com.mycompany.icp_2152_project.SimpleDataSource"%>
<%@page import="com.mycompany.icp_2152_project.QueryChecker"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.io.InputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Add Words</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
    </head>
    <body>
        <ul>
            <li><a href="InstructorPage.jsp">Home</a></li>
            <li><a href="InstructTestHistory.jsp">Test History</a></li>
            <li><a href="EditWords.jsp">Edit Words</a></li>
            <li><a href="DeleteWords.jsp">Delete Words</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <div class="login-page">
            <div class="form">
                <h3>Add Words</h3>
                <form action="AddWordsServlet" method="get">
                    <input placeholder="Welsh Word" name="welshWord"/>
                    <input placeholder="English Word" name="englishWord"/>
                    <input placeholder="M/F" name="gender"/>
                    <input placeholder="Meaning of the word" name="meaning"/>
                    <button name ="action" value="send" type="onlclick">Add</button>
                </form>
            </div>
        </div>
        <%@page import="java.sql.ResultSet"%>
        <%@page import="java.sql.Statement"%>
        <%@page import="java.sql.Connection"%>

        <%
            //Establish connection with database.properties
            
            Connection conn;
            ResultSet resultSet;
            PreparedStatement preparedStmt;

            try {
                InputStream stream = QueryChecker.class.getResourceAsStream("/database.properties");
                SimpleDataSource.init(stream);
            } catch (IOException e) {
                Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, e);
            }
        %>
        <h2 align="center"><font><strong>Current list of words</strong></font></h2>
        <table align="center" cellpadding="5" cellspacing="" border="1">
            <tr>

            </tr>
            <tr ID="TableHeader">
                <td><b>Word ID</b></td>
                <td><b>Welsh Word</b></td>
                <td><b>English Word</b></td>
                <td><b>gender</b></td>
                <td><b>meaning</b></td>
            </tr>
            <%
                //run query and diplay resuts in a table
                try {
                    conn = SimpleDataSource.getConnection();
                    String sql = "SELECT * FROM words";

                    preparedStmt = conn.prepareStatement(sql);
                    resultSet = preparedStmt.executeQuery();
                    while (resultSet.next()) {
            %>
            <tr ID="Results">

                <td><%=resultSet.getString("wordID")%></td>
                <td><%=resultSet.getString("welshWord")%></td>
                <td><%=resultSet.getString("englishWord")%></td>
                <td><%=resultSet.getString("gender")%></td>
                <td><%=resultSet.getString("meaning")%></td>

            </tr>

            <%
                    }
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            %>
        </table>
    </body>
</html>