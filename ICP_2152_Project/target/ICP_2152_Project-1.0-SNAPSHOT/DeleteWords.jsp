<%-- 
    Document   : DeleteWords
    Created on : 21-Apr-2017, 18:43:22
    Author     : Dion
--%>

<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.io.IOException"%>
<%@page import="com.mycompany.icp_2152_project.SimpleDataSource"%>
<%@page import="com.mycompany.icp_2152_project.QueryChecker"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.io.InputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
        <title>Delete Words</title>
    </head>
    <body>
        <ul>
            <li><a href="InstructorPage.jsp">Home</a></li>
            <li><a href="InstructTestHistory.jsp">Test History</a></li>
            <li><a href="AddWords.jsp">Add Words</a></li>
            <li><a href="EditWords.jsp">Edit Words</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <div class="login-page">
            <div class="form">
                <h3>Delete Words</h3>
                <form action="DeleteWordsServlet" method="get">
                    <input placeholder="Word ID" name="wordID"/>
                    <button name ="action" value="send" type="onlclick">Make Changes</button>
                </form>
            </div>
        </div>
         <%@page import="java.sql.ResultSet"%>
        <%@page import="java.sql.Statement"%>
        <%@page import="java.sql.Connection"%>

        <%
            //set up database connection
            
            Connection conn;
            ResultSet resultSet;
            PreparedStatement preparedStmt;

            try {
                InputStream stream = QueryChecker.class.getResourceAsStream("/database.properties");
                SimpleDataSource.init(stream);
            } catch (IOException e) {
                Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, e);
            }
        %>
        <h2 align="center"><font><strong>Current list of words</strong></font></h2>
        <table align="center" cellpadding="5" cellspacing="" border="1">
            <tr>

            </tr>
            <tr ID="TableHeader">
                <td><b>Word ID</b></td>
                <td><b>Welsh Word</b></td>
                <td><b>English Word</b></td>
                <td><b>gender</b></td>
                <td><b>meaning</b></td>
            </tr>
            <%
                //run sql query and display results in a table
                try {
                    conn = SimpleDataSource.getConnection();
                    String sql = "SELECT * FROM words";

                    preparedStmt = conn.prepareStatement(sql);
                    resultSet = preparedStmt.executeQuery();
                    while (resultSet.next()) {
            %>
            <tr ID="Results">

                <td><%=resultSet.getString("wordID")%></td>
                <td><%=resultSet.getString("welshWord")%></td>
                <td><%=resultSet.getString("englishWord")%></td>
                <td><%=resultSet.getString("gender")%></td>
                <td><%=resultSet.getString("meaning")%></td>

            </tr>

            <%
                    }
                    conn.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            %>
        </table>
    </body>
</html>
