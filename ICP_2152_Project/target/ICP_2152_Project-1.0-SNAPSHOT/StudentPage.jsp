<%-- 
    Document   : StudentPage
    Created on : 19-Apr-2017, 19:26:52
    Author     : Dion
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Page</title>
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <ul>
            <li><a href="TakeTestServlet">Take Test</a></li>
            <li><a href="TestHistory.jsp">Test History</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>   
        <%
            if (session.getAttribute("username") == null) {
                response.sendRedirect("StudentLogin.xhtml");
            }
        %>
        <div class="login-page">
            <div class="form">
                Welcome: ${username}
            </div>
        </div>
    </body>
</html>