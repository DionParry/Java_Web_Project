/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Anthony
 * Created: 06-Apr-2017
 */
INSERT INTO words (welshWord, englishWord, gender, meaning)
VALUES 
('arth', 'bear', 'f', 'Hairy land mammel which walks on the soles of its feet. '),
('tarw', 'bull', 'm', 'An uncastrated male bovine animal.'),
('cath', 'cat', 'f', 'A small domesticated carnivorous mammal with soft fur.'),
('buwch', 'cow', 'f', 'A fully grown female animal of a domesticated breed of ox.'),
('carw', 'deer', 'm', 'A hoofed grazing or browsing animal.'),
('ci', 'dog', 'm', 'A domesticated carnivorous mammal that typically has a long snout.'),
('asyn', 'donkey', 'm', 'A domesticated hoofed mammal of the horse family with long ears and a braying call.'),
('eryr', 'eagle', 'm', 'A large bird of prey with a massive hooked bill and long broad wings.'),
('eliffant', 'elephant', 'm', 'A very large plant-eating mammal with a prehensile trunk.'),
('jiraff', 'giraffe', 'm', 'A large African mammal with a very long neck and forelegs.'),
('gafr', 'goat', 'f', 'A hardy domesticated ruminant mammal that has backward-curving horns.'),
('ceffyl', 'horse', 'm', 'A solid-hoofed plant-eating domesticated mammal with a flowing mane and tail.'),
('llew', 'lion', 'm', 'A large tawny-coloured cat that lives in prides, found in Africa and NW India.'),
('simach', 'monkey', 'm', 'A small to medium-sized primate that typically has a long tail.'),
('cwningen', 'rabbit', 'f', 'A gregarious burrowing plant-eating mammal, with long ears.'),
('neidr', 'snake', 'f', 'A long limbless reptile which has no eyelids.'),
('teigr', 'tiger', 'm', 'Large cat that can found in jungles.'),
('blaidd', 'wolf', 'm', 'A wild carnivorous mammal which is the largest member of the dog family.'),
('mab', 'son', 'm', 'A boy or man in relation to either or both of his parents.'),
('merch', 'daugther', 'f', ' girl or woman in relation to either or both of her parents.'),
('mam', 'mother', 'f', 'A woman in relation to her child or children.'),
('tad', 'father', 'm', 'A man in relation to his child or children.'),
('ewythr', 'uncle', 'm', "The brother of one's father or mother or the husband of one's aunt."),
('modryb', 'auntie', 'f', 'Informal term for aunt.'),
('gwr', 'husband', 'm', 'A married man considered in relation to his spouse.'),
('gwraig', 'wife', 'f', 'A married woman considered in relation to her spouse.'),
('gwely', 'bed', 'm', 'A piece of furniture for sleep or rest, typically a framework with a mattress.'),
('cadair', 'chair', 'f', 'A separate seat for one person, typically with a back and four legs.'),
('cibyn', 'cup', 'm', 'A small bowl-shaped container for drinking from, typically having a handle.'),
('het', 'hat', 'f', 'A shaped covering for the head worn for warmth, as a fashion item, or as part of a uniform.'),
('fforc', 'fork', 'f', 'An implement with two or more prongs used for lifting food to the mouth or holding it when cutting.'),
('cyllel', 'knife', 'f', 'An instrument composed of a blade fixed into a handle, used for cutting or as a weapon.'),
('inc', 'ink', 'm', 'A coloured fluid or paste used for writing, drawing, printing, or duplicating.'),
('siaced', 'jacket', 'f', 'An outer garment extending either to the waist or the hips, typically having sleeves and a fastening down the front.'),
('llusern', 'lamp', 'f', 'A device for giving light.'),
('map', 'map', 'f', 'A diagrammatic representation of an area of land or sea showing physical features.'),
('dysgl', 'plate', 'f', 'A flat dish, typically circular and made of china, from which food is eaten or served.'),
('sebon', 'soap', 'm', 'A substance used with water for washing and cleaning.'),
('llwy', 'spoon', 'f', 'An implement consisting of a small, shallow oval or round bowl on a long handle, used for eating, stirring, and serving food.'),
('ysgrepan', 'wallet', 'f', 'A pocket-sized flat folding case for holding money and plastic cards.');

