<%-- 
    Document   : InstructTestHistory
    Created on : 20-Apr-2017, 21:39:00
    Author     : Anthony
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.IOException"%>
<%@page import="com.mycompany.icp_2152_project.SimpleDataSource"%>
<%@page import="com.mycompany.icp_2152_project.QueryChecker"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
        div {
            width: 30%;
            height: 100%;
            text-align: center;
            margin: auto;
        }

        .tb {
            margin: auto;
        }
    </style>
    <head>
        <title>Test History</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
    </head>
    <body>
        <ul>
            <li><a href="AdminPage.jsp">Home</a></li>
            <li><a href="CreateAccount.xhtml">Create Student Account</a></li>
            <li><a href="CreateInstructorAccount.xhtml">Create Instructor Account</a></li>
            <li><a href="ResetPassword.xhtml">Reset Account Password</a></li>
            <li><a href="DeleteAccount.xhtml">Delete Account</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <div class="wrapHistory">
            <h1>Test History</h1>
            </br>
            <form>
                <input  placeholder="username" name="username"/>
            </form>

            <%@page import="java.sql.DriverManager"%>
            <%@page import="java.sql.ResultSet"%>
            <%@page import="java.sql.Statement"%>
            <%@page import="java.sql.Connection"%>
            <%
                Connection conn;
                ResultSet resultSet;
                PreparedStatement preparedStmt;

                try {
                    InputStream stream = QueryChecker.class.getResourceAsStream("/database.properties");
                    SimpleDataSource.init(stream);
                } catch (IOException e) {
                    Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, e);
                }

                if (session.getAttribute("username") == "") {
                    response.sendRedirect("AdminPage.jsp");
                }

            %>

            <table class="tb">
                <tr>

                </tr>
                <tr ID="TableHeader">
                    <td><b>Score</b></td>
                    <td><b>Completion Date</b></td>
                </tr>
                <%                
                    String name = request.getParameter("username");
                    try {
                        String sql = "SELECT * FROM results where username = ?;";
                        conn = SimpleDataSource.getConnection();
                        preparedStmt = conn.prepareStatement(sql);

                        preparedStmt = conn.prepareStatement(sql);
                        preparedStmt.setString(1, name);
                        resultSet = preparedStmt.executeQuery();

                        //resultSet = statement.executeQuery(sql);
                        while (resultSet.next()) {
                %>
                <tr ID="Results">

                    <td><%=resultSet.getString("score")%>/20</td>
                    <td><%=resultSet.getString("completionDate")%></td>

                </tr>

                <%
                        }
                        conn.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                %>
            </table></div>
    </body>
</html>
