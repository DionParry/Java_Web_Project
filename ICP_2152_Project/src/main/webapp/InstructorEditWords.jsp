<%-- 
    Document   : EditWords
    Created on : 19-Apr-2017, 12:16:02
    Author     : Anthony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<?xml version="1.0" encoding="UTF-8"?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Edit Words</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" type="text/css" href="loginStyle.css"/>
        <link rel="stylesheet" type="text/css" href="style.css"/> 
    </head>
    <body>
        <ul>
            <li><a href="InstructorPage.jsp">Home</a></li>
            <li><a href="TakeTestServlet">Take Test</a></li>
            <li><a href="TestHistory.jsp">Test History</a></li>
            <li><a href="EditWords.jsp">Edit Words</a></li>
            <li><a href="LogoutServlet">Logout</a></li>
        </ul>
        <div class="login-page">
            <div class="form">
                <h3>Edit Words</h3>
                <form action="EditWordsServlet" method="get">
                    <input placeholder="Word ID" name="wordID"/>
                    <input placeholder="Welsh Word" name="welshWord"/>
                    <input placeholder="English Word" name="englishWord"/>
                    <input placeholder="M/F" name="gender"/>
                    <input placeholder="Meaning of the word" name="meaning"/>
                    <button name ="action" value="send" type="onlclick">Make Changes</button>
                </form>
            </div>
        </div>
        <%@page import="java.sql.DriverManager"%>
        <%@page import="java.sql.ResultSet"%>
        <%@page import="java.sql.Statement"%>
        <%@page import="java.sql.Connection"%>

        <%
            //String id = request.getParameter("userId");
            String driverName = "com.mysql.jdbc.Driver";
            String connectionUrl = "jdbc:mysql://localhost:3306/";
            String dbName = "icp-2152";
            String userId = "root";
            String password = "";

            try {
                Class.forName(driverName);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;
        %>
        <h2 align="center"><font><strong>Current list of words</strong></font></h2>
        <table align="center" cellpadding="5" cellspacing="" border="1">
            <tr>

            </tr>
            <tr ID="TableHeader">
                <td><b>Word ID</b></td>
                <td><b>Welsh Word</b></td>
                <td><b>English Word</b></td>
                <td><b>gender</b></td>
                <td><b>meaning</b></td>
            </tr>
            <%
                try {
                    connection = DriverManager.getConnection(connectionUrl + dbName, userId, password);
                    statement = connection.createStatement();
                    String sql = "SELECT * FROM words";

                    resultSet = statement.executeQuery(sql);
                    while (resultSet.next()) {
            %>
            <tr ID="Results">

                <td><%=resultSet.getString("wordID")%></td>
                <td><%=resultSet.getString("welshWord")%></td>
                <td><%=resultSet.getString("englishWord")%></td>
                <td><%=resultSet.getString("gender")%></td>
                <td><%=resultSet.getString("meaning")%></td>

            </tr>

            <%
                    }
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            %>
        </table>
    </body>
</html>