/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * . This class is the main class to enable the user to take the test.
 * @author jorda
 */
public class TakeTestServlet extends HttpServlet {

    static int totalScore = 0;
    Test a = new Test();

    String questionType1 = "What is the gender for the Welsh noun";
    String questionType2 = "What is the meaning of the Welsh noun";
    String questionType3 = "What is the Welsh noun for the english word";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    LinkedList<String> welshWords = new LinkedList<>();
    LinkedList<String> englishWords = new LinkedList<>();
    LinkedList<String> meaning = new LinkedList<>();
    LinkedList<String> gender = new LinkedList<>();

    Random rand = new Random();
    public static String Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, Q13,
            Q14, Q15, Q16, Q17, Q18, Q19, Q20;

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            HttpSession session = request.getSession();
            if (session.getAttribute("username") == null) {
                response.sendRedirect("StudentLogin.xhtml");
            }
            a.abc(welshWords, englishWords, gender, meaning);

            int[] numbers = new Random().ints(1, 40).distinct().limit(21).toArray();

            //Calls the method for each Question
            Q1 = (String) welshWords.get(numbers[1]);
            Q2 = (String) welshWords.get(numbers[2]);
            Q3 = (String) welshWords.get(numbers[3]);
            Q4 = (String) welshWords.get(numbers[4]);
            Q5 = (String) welshWords.get(numbers[5]);

            Q6 = (String) welshWords.get(numbers[6]);
            Q7 = (String) welshWords.get(numbers[7]);
            Q8 = (String) welshWords.get(numbers[8]);
            Q9 = (String) welshWords.get(numbers[9]);
            Q10 = (String) welshWords.get(numbers[10]);

            Q11 = (String) englishWords.get(numbers[11]);
            Q12 = (String) englishWords.get(numbers[12]);
            Q13 = (String) englishWords.get(numbers[13]);
            Q14 = (String) englishWords.get(numbers[14]);
            Q15 = (String) englishWords.get(numbers[15]);

            Q16 = (String) welshWords.get(numbers[16]);
            Q17 = (String) welshWords.get(numbers[17]);
            Q18 = (String) welshWords.get(numbers[18]);
            Q19 = (String) welshWords.get(numbers[19]);
            Q20 = (String) englishWords.get(numbers[20]);
            /* 
            This prints the html to a webpage.Also displaying the questions the user will have to answer
            . */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println(" <head>\n"
                    + "        <title>TODO supply a title</title>\n"
                    + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n"
                    + "        <link rel=\"stylesheet\" type=\"text/css\" href=\"testStyle.css\"/>\n"
                    + "        \n"
                    //  + "        <style>ul{list-style-type: none;}</style>\n"
                    + "    </head>\n"
                    + "    <body>\n"
                    + "        <div class=\"page-wrap\">\n"
                    + "            <h1>Welsh Test</h1>\n"
                    + "            <form action=\"ResultsServlet\" method=\"get\" id=\"quiz\">\n"
            );
            out.println(Question(Q1, "answer1", questionType1));
            out.println(Question(Q2, "answer2", questionType1));
            out.println(Question(Q3, "answer3", questionType1));
            out.println(Question(Q4, "answer4", questionType1));
            out.println(Question(Q5, "answer5", questionType1));
            out.println(Question(Q16, "answer16", questionType1));
            out.println(Question(Q17, "answer17", questionType1));

            out.println(Question2(Q6, "answer6", questionType2, numbers[6], meaning));
            out.println(Question2(Q7, "answer7", questionType2, numbers[7], meaning));
            out.println(Question2(Q8, "answer8", questionType2, numbers[8], meaning));
            out.println(Question2(Q9, "answer9", questionType2, numbers[9], meaning));
            out.println(Question2(Q10, "answer10", questionType2, numbers[10], meaning));
            out.println(Question2(Q18, "answer18", questionType2, numbers[18], meaning));
            out.println(Question2(Q19, "answer19", questionType2, numbers[19], meaning));

            out.println(Question3(Q11, "answer11", "gm1", questionType3, numbers[11], welshWords));
            out.println(Question3(Q12, "answer12", "gm2", questionType3, numbers[12], welshWords));
            out.println(Question3(Q13, "answer13", "gm3", questionType3, numbers[13], welshWords));
            out.println(Question3(Q14, "answer14", "gm4", questionType3, numbers[14], welshWords));
            out.println(Question3(Q15, "answer15", "gm5", questionType3, numbers[15], welshWords));
            out.println(Question3(Q20, "answer20", "gm6", questionType3, numbers[20], welshWords));

            out.print(
                    "<input name=\"button\" type=\"submit\" value=\"Submit Test\" />\n"
                    + "            </form>\n"
                    + "        </div>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     *  This is used for the gender mutation question
     * @param a The noun
     * @param answer The Answer. which is used when getting the attribute for validation
     * @param question The qestion type
     * @return
     */
    public String Question(String a, String answer, String question) {

        return "                <ul style=\"background-color:white;\" class=\"list\">\n"
                + "                    <li >\n"
                + "                        <h3>" + question + ":>     " + a + "</h3>\n"
                + "                        <div>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"question\" value=\"m\" />\n"
                + "                            <label for=\"question-1-answers-A\">Masculine</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"question-1-answers-B\" value=\"f\" />\n"
                + "                            <label for=\"question-1-answers-B\">Feminine</label>\n"
                + "                        </div>\n"
                + "                    </li>\n"
                + "                    \n"
                + "                </ul></br>\n";

    }

    Random rndSelect = new Random();

    /**
     * This is used for the second type of question. This method uses random number generation to
     * grab random answer from a linked list contain data from the database. One of the answers is the correct one
     * @param f This is the noun to be shown
     * @param answer This is to name the input so one can retrieve them later on
     * @param question this is the question type
     * @param number This is used to get a random value from the Linked lists
     * @param theList This is the list to be used (e.g. welshWords)
     * @return
     */
    public String Question2(String f, String answer, String question, int number, LinkedList theList) {
//        String rightAnswer = a.meaningOfNoun(number, theList);
//        String rightAnswer = (String) theList.get(number);
        List answers = new ArrayList();
        answers.add(theList.get(number));
        Boolean uniqueWord = false;
        int n1, n2;
        Random r1 = new Random();
        while (uniqueWord.equals(false)) {
            n1 = r1.nextInt(20) + 1;
            n2 = r1.nextInt(20) + 1;
            if (n1 != number && n2 != number) {
                answers.add(a.meaningOfNoun(n1, theList));
                answers.add(a.meaningOfNoun(n2, theList));
                uniqueWord = true;
            }
        }

        Collections.shuffle(answers);
        return "                <ul style=\"background-color:white;\" class=\"list\">\n"
                + "                    <li >\n"
                + "                        <h3>" + question + ":>     " + f + "</h3>\n"
                + "                        <div>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerA\" value=\"" + answers.get(1) + "\" />\n"
                + "                            <label for=\"Q1-AnswerA\">" + answers.get(1) + "</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerB\" value=\"" + answers.get(2) + "\" />\n"
                + "                            <label for=\"Q1-AnswerB\">" + answers.get(2) + "</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerB\" value=\"" + answers.get(0) + "\" />\n"
                + "                            <label for=\"Q1-AnswerB\">" + answers.get(0) + "</label></br>\n"
                + "                        </div>\n"
                + "                    </li>\n"
                + "                    \n"
                + "                </ul></br>\n";

    }

    /**
     * This method is used for question type 3. This is similar to Question2 however, it has an addition that is
     * the gender marker. 
     * @param f The noun
     * @param answer Name the input to retrieve user answer later one
     * @param gMarker The gender marker of the noun itself
     * @param question The question type to be asked
     * @param number The random int which will pull data from a linked list
     * @param theList The list to retrieve the data from
     * @return
     */
    public String Question3(String f, String answer, String gMarker, String question, int number, LinkedList theList) {
//        String rightAnswer = a.meaningOfNoun(number, theList);
//        String rightAnswer = (String) theList.get(number);
        List answers = new ArrayList();
        answers.add(theList.get(number));
        Boolean uniqueWord = false;
        int n1, n2;
        Random r1 = new Random();
        while (uniqueWord.equals(false)) {
            n1 = r1.nextInt(10) + 1;
            n2 = r1.nextInt(20) + 11;
            if (n1 != number && n2 != number) {
                answers.add(a.meaningOfNoun(n1, theList));
                answers.add(a.meaningOfNoun(n2, theList));
                uniqueWord = true;
            }
        }
        Collections.shuffle(answers);
        return "                <ul style=\"background-color:white;\" class=\"list\">\n"
                + "                    <li >\n"
                + "                        <h3>" + question + ":>     " + f + "</h3>\n"
                + "                        <div>\n"
                + "                        <p class=\"subQuestionTitle\">Welsh Noun:</p>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerA\" value=\"" + answers.get(1) + "\" />\n"
                + "                            <label for=\"Q1-AnswerA\">" + answers.get(1) + "</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerB\" value=\"" + answers.get(2) + "\" />\n"
                + "                            <label for=\"Q1-AnswerB\">" + answers.get(2) + "</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + answer + "\" id=\"Q1-AnswerB\" value=\"" + answers.get(0) + "\" />\n"
                + "                            <label for=\"Q1-AnswerB\">" + answers.get(0) + "</label></br>\n"
                + "                        </div>\n"
                + "                        <div>\n"
                + "                        <p class=\"subQuestionTitle\">Gender marker:</p>\n"
                + "                            <input type=\"radio\" name=\"" + gMarker + "\" id=\"question\" value=\"m\" />\n"
                + "                            <label for=\"question-1-answers-A\">Masculine</label></br>\n"
                + "                            <input type=\"radio\" name=\"" + gMarker + "\" id=\"question-1-answers-B\" value=\"f\" />\n"
                + "                            <label for=\"question-1-answers-B\">Feminine</label>\n"
                + "                        </div>\n"
                + "                    </li>\n"
                + "                    \n"
                + "                </ul></br>\n";

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}