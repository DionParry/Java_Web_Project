/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.sql.*;
import java.io.*;
import java.sql.ResultSet;

/**
 *
 * @author Dion
 */
public class MySqlTester {

    /**
     * Tests to see if a connection has been established
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void main(String[] args) throws IOException,
            ClassNotFoundException, SQLException {
        
        //SimpleDataSource.init("D:\\Documents\\Uni\\Year 2\\ICP 2152 - Java Technologies\\HelloMaven\\mavenproject1\\src\\test\\java\\com\\mycompany\\mavenproject1\\database.properties");
        InputStream stream = MySqlTester.class.getResourceAsStream("/database.properties");
        SimpleDataSource.init(stream);
        Connection conn = SimpleDataSource.getConnection();
        Statement st = conn.createStatement();
        try {
            st.execute("CREATE TABLE IF NOT EXISTS accounts (balance DECIMAL(5,2))");
            st.execute("INSERT INTO accounts VALUES (999.99)");
            st.execute("INSERT INTO accounts VALUES (666.66)");

            ResultSet rs = st.executeQuery("SELECT * FROM accounts");

            while (rs.next()) {
                System.out.println(rs.getString("balance"));
            }

            //st.execute("DROP TABLE accounts");
        } finally {
            System.out.println("Table created and then dropped!");
            st.close();
            conn.close();
        }
    }
}
