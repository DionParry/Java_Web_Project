/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author Dion
 */
public class PasswordHash {

    static Map<String, String> DB = new HashMap<>();

    /**
     *
     */
    public static final String SALT = "my-salt-text";
    QueryChecker check = new QueryChecker();

    /**
     *
     * @param password
     * @return
     */
    public String hashPassword(String password) {
        String tempPassword = password;
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5 - secure one way hash functions
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(tempPassword.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
        }
        System.out.println(generatedPassword);
        return generatedPassword;
    }
    
    /**
     *
     * @param username
     * @param password
     */
    public void signUp(String username, String password) {
        String saltedPassword = SALT + password;
        String hashedPassword = generateHash(saltedPassword);
        check.insertLessData(username, hashedPassword);
        DB.put(username, hashedPassword);

    }

    /**
     *
     * @param username
     * @param password
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Boolean login(String username, String password) throws IOException, ClassNotFoundException, SQLException {
        Boolean connected = false;
        // remember to use the same SALT value use used while storing password for the first time
        String saltedPassword = SALT + password;
        String hashedPassword = generateHash(saltedPassword);
        // String tempPassword = check.getPassword(username);
        String storedPasswordHash = DB.get(username);
        if (hashedPassword.equals(storedPasswordHash)) {
            connected = true;
        } else {
            connected = false;
        }
        return connected;
    }

    /**
     *
     * @param password
     * @return
     */
    public String generateHash(String password) {
        StringBuilder hash = new StringBuilder();
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(password.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
        }
        return hash.toString();
    }

    /**
     * Validation check of password
     * @param password
     * @return a true or false value on whether 
     * the password passed the validation check
     */
    public Boolean validatePassword(String password) {
        Boolean pass = false;
        Pattern p1 = Pattern.compile("[a-z]");
        Pattern p2 = Pattern.compile("[A-Z]");
        Pattern p3 = Pattern.compile("[0-9]");
        Pattern p4 = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
        if (p1.matcher(password).find() && p2.matcher(password).find() 
                && p3.matcher(password).find() && !p4.matcher(password).find()
                && password.length() > 3 && password.length() <= 8){
            pass = true;
        } 
        return pass;
    }

    /**
     *
     * @param args
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws SQLException
     */
    public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {
        PasswordHash hash = new PasswordHash();
        QueryChecker check = new QueryChecker();
        //hash.signUp("root", "root");
        /*
        if (hash.login("root", "root")) {
            System.out.println("Login Successful");
        } else {
            System.out.println("Error, username or password incorrect");
        }
         */

       // String password = "number123";
        String name = "Teacher1";
        String password = "Teacher1";
        String p1 = "Teacher1";
        //boolean valid = hash.validatePassword(password);
        //System.out.println("Valid Password: "+valid);
        //System.out.println(JakePassword);
        //JakePassword = hash.hashPassword(JakePassword);
            //System.out.println(check.getPassword("instructor", name));

        /*
            name = "Jordan";
            password = "H0es";
            System.out.println(check.getPassword("student", name));
            System.out.println(hash.hashPassword(password));
        if (check.getPassword("student",name).equals(hash.hashPassword(password))) {
            System.out.println("match");
        } */
    }
}
