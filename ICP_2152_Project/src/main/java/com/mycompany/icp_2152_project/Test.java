/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorda
 */
public class Test {

    /**
     * establishes database connection
     */
    public Test() {
        try {
            InputStream stream = Test.class.getResourceAsStream("/database.properties");
            SimpleDataSource.init(stream);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Test a = new Test();
       // a.abc();
    }

    /**
     * Use the 'wordsToList' method and add data to specific LinkedList
     * @param l1
     * @param l2
     * @param l3
     * @param l4
     */
    public void abc(LinkedList l1, LinkedList l2, LinkedList l3, LinkedList l4) {
        wordsToList(l1, "welshWord");
        wordsToList(l2, "englishWord");
        wordsToList(l3, "gender");
        wordsToList(l4, "meaning");

    }

    PreparedStatement preparedStmt;
    ResultSet resultSet;
    Connection conn;

    /**
     * Executes query nd saves data to an array list
     * @param list The list to add the data to
     * @param column The colum to select the data from in the database table
     */

    public void wordsToList(LinkedList list, String column) {
        String query = "select * from words";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                //r = rand.nextInt(40) + 1;
                list.add(rs.getString(column));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    /**
     * Returns the meaning of a noun, using a specific linkedlist
     * @param a
     * @param theList
     * @return
     */
    public String meaningOfNoun(int a, LinkedList theList) {
        String ans = (String) theList.get(a);
        return ans;
    }

}