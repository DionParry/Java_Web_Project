package com.mycompany.icp_2152_project;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
    
/**
 *
 * @author Dion
 */
@SuppressWarnings("WeakerAccess")
public class Email
{
	private String emailSubject = "Financial Update";
	
	/**
	 * Sends email
	 */
	public void sendMessage()
	{
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication("email","password");
			}
		});
		
		try
		{
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("email"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("anthonynorman1988@gmail.com"));
			message.setSubject("Financial Update");
			setDate();
			message.setContent(emailTemplate, "text/html; charset=utf-8");
			
			Transport.send(message);
		} catch (MessagingException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets email summary section
	 * @param summary summary content
	 */
	public void setSummary(String summary)
	{
		emailTemplate = emailTemplate.replace("#Summary", summary);
	}
	
	/**
	 * Sets email monitoring section
	 * @param monitorURL monitored URLs
	 */
	public void setMonitorURL(String monitorURL)
	{
		emailTemplate = emailTemplate.replace("#MonitoringURL", monitorURL);
	}
	
	/**
	 * Sets email context section
	 * @param contextWordsAndCompanies contextual information to set
	 */
	public void setContextWordsAndCompanies(String contextWordsAndCompanies)
	{
		emailTemplate = emailTemplate.replace("#Context", contextWordsAndCompanies);
	}
	
	/**
	 * Adds timestamp to email
	 */
	public void setDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		emailTemplate = emailTemplate.replace("#Date", dateFormat.format(date));
	}
	
        private String emailTemplate = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" + "<title>Financial Update</title>\n" + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" + "</head>\n" + "<body style=\"margin: 0; padding: 0; background-color: #F5F5F5\">\n" + "\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\t\n" + "\t\t<tr>\n" + "\t\t\t<td style=\"padding: 10px 0 30px 0;\">\n" + "\t\t\t\t<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"800\" style=\"border: 0px solid #cccccc; border-collapse: collapse;\">\n" + "\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t<td align=\"center\" bgcolor=\"#70bbd9\" style=\"padding: 30px 0 30px 0; color: #153643; font-size: 26px; font-weight: bold; font-family: Arial, sans-serif;\">\n" + "\t\t\t\t\t\t\tFinancial Update\n" + "\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t</tr>\n" + "\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t<td bgcolor=\"#F5F5F5\" style=\"padding: 40px 30px 40px 30px;\">\n" + "\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + "\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t<td style=\"color: #153643; font-family: Arial, sans-serif; font-size: 19px;\">\n" + "\t\t\t\t\t\t\t\t\t\t<b>Monitoring:#MonitoringURL</b>\n" + "\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t<td style=\"padding: 0px 0 10px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;\">\n" + "\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t<td>\n" + "\t\t\t\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + "\t\t\t\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t<td width=\"260\" valign=\"top\" style=\"padding: 0 10px 0 0; border-right: 1px solid #cccccc; border-collapse: collapse;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"padding: 20px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 17px; line-height: 20px;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tSummary:\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"padding: 15px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#Summary\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t</table>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t<td width=\"260\" valign=\"top\" style=\"padding: 0 0 10px 10px;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"padding: 20px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 17px; line-height: 20px;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tContext:\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"padding: 15px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;\">\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t#Context\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t\t</table>\n" + "\t\t\t\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t\t\t\t</table>\n" + "\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t</table>\n" + "\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t</tr>\n" + "\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t<td bgcolor=\"#ee4c50\" style=\"padding: 30px 30px 30px 30px;\">\n" + "\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n" + "\t\t\t\t\t\t\t\t<tr>\n" + "\t\t\t\t\t\t\t\t\t<td style=\"color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;\" width=\"75%\">\n" + "\t\t\t\t\t\t\t\t\t\tThis is an automated financial update<br>\n" + "\t\t\t\t\t\t\t\t\t\tSent at: #Date\n" + "\t\t\t\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t\t\t\t</tr>\n" + "\t\t\t\t\t\t\t</table>\n" + "\t\t\t\t\t\t</td>\n" + "\t\t\t\t\t</tr>\n" + "\t\t\t\t</table>\n" + "\t\t\t</td>\n" + "\t\t</tr>\n" + "\t</table>\n" + "</body>\n" + "</html>";
	
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
            Email email = new Email();
            email.sendMessage();
        }
}
