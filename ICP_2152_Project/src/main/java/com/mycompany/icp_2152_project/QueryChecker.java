/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dion
 */
public class QueryChecker {

    PreparedStatement preparedStmt;
    ResultSet resultSet;
    Connection conn;
    LinkedList welshWords = new LinkedList();
    LinkedList englishWords = new LinkedList();
    LinkedList meanings = new LinkedList();
    LinkedList genders = new LinkedList();

    /**
     * Establishes database properties for connection
     */
    public QueryChecker() {
        try {
            InputStream stream = QueryChecker.class.getResourceAsStream("/database.properties");
            SimpleDataSource.init(stream);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * sets the student data into database
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     */
    public void setData(String param1, String param2, String param3, String param4) {
        PasswordHash hash = new PasswordHash();
        param2 = hash.hashPassword(param2);
        try {
            String query = " insert into student (username, password, dateOfBirth, email)"
                    + " values (?, ?, ?, ?);";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, param1);
            preparedStmt.setString(2, param2);
            preparedStmt.setString(3, param3);
            preparedStmt.setString(4, param4);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * sets admin data into database
     * @param username
     * @param password
     */
    public void setAdminData(String username, String password) {
        PasswordHash hash = new PasswordHash();
        password = hash.hashPassword(password);
        try {
            String query = " insert into admin (username, password)"
                    + " values (?, ?);";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            preparedStmt.setString(2, password);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sets Instructor data into datasbase
     * @param username
     * @param password
     * @param email
     */
    public void setInstructorData(String username, String password, String email) {
        PasswordHash hash = new PasswordHash();
        password = hash.hashPassword(password);
        try {
            String query = " insert into instructor (username, password, email)"
                    + " values (?, ?, ?);";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            preparedStmt.setString(2, password);
            preparedStmt.setString(3, email);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * returns a students details
     * @param param1
     * @param param2
     * @throws SQLException
     */
    public void getData(String param1, String param2) throws SQLException {
        String query = " select (username, password) values (?, ?) from student";
        conn = SimpleDataSource.getConnection();
        preparedStmt = conn.prepareStatement(query);
        preparedStmt.setString(1, param1);
        preparedStmt.setString(2, param2);
        preparedStmt.execute();
    }

    /**
     * returns the users password
     * @param user
     * @param username
     * @return
     */
    public String getPassword(String user, String username) {
        String query = "SELECT password From " + user + " where username=?";
        String a = "";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                a = rs.getString("password");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    /**
     * deletes an account from the database
     * @param accountType
     * @param username
     */
    public void deleteAccount(String accountType, String username) {
        try {
            String query = " delete from " + accountType + " where username = ?;";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * retrieves instructors password from database
     * @param username
     * @return
     */
    public String getInstructorPassword(String username) {
        String query = "select password from instructor where username=?";
        String a = "";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                a = rs.getString("password");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    /**
     * validates if the user exists
     * @param username
     * @return
     */
    public String userValidation(String username) {
        String query = "SELECT username From student where username=?";
        String a = "";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                a = rs.getString("username");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return a;
    }

    /**
     *  inserts data into student table
     * @param param1
     * @param param2
     */
    public void insertLessData(String param1, String param2) {
        String query = " insert into student (username, password)"
                + " values (?, ?);";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, param1);
            preparedStmt.setString(2, param2);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * changes the students password
     * @param password
     * @param username
     */
    public void changePassword(String password, String username) {
        PasswordHash hash = new PasswordHash();
        password = hash.hashPassword(password);
        try {
            String query = " update student set password = ? where username = ?;";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, password);
            preparedStmt.setString(2, username);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Resets the users password bases on their account type
     * @param accountType
     * @param username
     * @param password
     */
    public void resetPassword(String accountType, String username, String password) {
        PasswordHash hash = new PasswordHash();
        password = hash.hashPassword(password);
        try {
            String query = " update " + accountType + " set password = ? where username = ?;";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, password);
            preparedStmt.setString(2, username);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adds data into the words table
     * @param welshWord
     * @param englishWord
     * @param gender
     * @param meaning
     */
    public void addWords(String welshWord, String englishWord, String gender, String meaning) {
        String query = " insert into words (welshWord, englishWord, gender, meaning)"
                + " values (?, ?, ?, ?);";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, welshWord);
            preparedStmt.setString(2, englishWord);
            preparedStmt.setString(3, gender);
            preparedStmt.setString(4, meaning);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Edits the data in the database
     * @param wordID
     * @param welshWord
     * @param englishWord
     * @param gender
     * @param meaning
     */
    public void editWords(String wordID, String welshWord, String englishWord, String gender, String meaning) {
        try {
            String query = "update words set welshWord = ?, englishWord = ?, gender = ?, meaning = ? where wordID = ?;";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, welshWord);
            preparedStmt.setString(2, englishWord);
            preparedStmt.setString(3, gender);
            preparedStmt.setString(4, meaning);
            preparedStmt.setString(5, wordID);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Uses wordID to remove word from database
     * @param wordID 
     */
    public void deleteWords(String wordID) {
         try {
            String query = " delete from words where wordID = ?;";
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, wordID);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Display the current words in the database
     */
    public void displayWords() {
        String query = "select * from words";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            System.out.println("\nTable Contents" + "\n*****************");
            System.out.println("wordID\twelshWords\tenglishWords\tgender\tmeaning");
            while (rs.next()) {
                int id = rs.getInt("wordID");
                String welsh = rs.getString("welshWord");
                String english = rs.getString("englishWord");
                String gender = rs.getString("gender");
                String meaning = rs.getString("meaning");
                System.out.println(id + "\t" + welsh + "\t\t" + english + "\t\t" + gender + "\t" + meaning);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Retrieves the data from words and stores into linked lists
     */
    public void getAllWords() {
        String query = "select * from words";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("wordID");
                String welsh = rs.getString("welshWord");
                String english = rs.getString("englishWord");
                String gender = rs.getString("gender");
                String meaning = rs.getString("meaning");
                welshWords.add(welsh);
                englishWords.add(english);
                genders.add(gender);
                meanings.add(meaning);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("welsh words " + welshWords);
        System.out.println("english words " + englishWords);
        System.out.println("gender type " + genders);
        System.out.println("meaning " + meanings);
    }

    /**
     * Returns the words from a desired column and adds it to list
     * @param list
     * @param column
     */
    public void getWords(LinkedList list, String column) {
        String query = "select * from words";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                String data = rs.getString(column);
                list.add(data);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * sets the linked lists
     */
    public void setLists() {
        getWords(welshWords, "welshWord");
        getWords(englishWords, "englishWord");
        getWords(meanings, "meaning");
        getWords(genders, "gender");
        Random rand = new Random();
        int r = rand.nextInt(40) + 1;
        System.out.println(welshWords.get(r));
        System.out.println(englishWords.get(r));
        System.out.println(meanings.get(r));
        System.out.println(genders.get(r));
    }

    /**
     * checks the gender of the word
     * @param noun
     * @return
     */
    public String checkGender(String noun) {
        String query = "SELECT gender FROM words WHERE welshWord=?";
        String g = "";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, noun);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                g = rs.getString("gender");
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return g;
    }

    /**
     * checks users answer
     * @param column
     * @param noun
     * @param nounColumn
     * @return
     */
    public String checkAnswer(String column, String noun, String nounColumn) {
        String query = "SELECT " + column + " FROM words WHERE " + nounColumn + "=?";
        String s = "";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, noun);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                s = rs.getString(column);
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
    }

    /**
     * stores the users test results into table
     * @param username
     * @param score
     */
    public void storeResults(String username, int score) {
        String query = " insert into results (username, score)"
                + " values (?, ?);";
        try {
            conn = SimpleDataSource.getConnection();
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, username);
            preparedStmt.setInt(2, score);
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(QueryChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        QueryChecker check = new QueryChecker();
        //  PasswordHash hash = new PasswordHash();
//        check.setData("dion", "123", "1996-10-09", "dion@gmail.com");
//        System.out.println(check.getPassword("xasxa"));
//        System.out.println(check.userValidation("xasxa"));
//        String newPassword = "123";
//        hash.hashPassword(newPassword);
//        check.changePassword(newPassword, "dion");
//        check.addWords("ci", "dog", "M", "an animal");
//        check.displayWords();
//        check.setInstructorData("jordan", "123", "jordan@gmail.com");
//        check.setAdminData("God", "123");
//        check.editWords("2", "neidr", "snake", "M", "a reptile");
//        check.getPassword("admin", "King dom admin");
//        check.getPassword("dion");
          check.setAdminData("a", "a");
//        check.getAllWords();  
//        check.setLists();

        // check.deleteAccount("student", "hi");
      
//        String gender = check.checkAnswer("gender", "ci", "welshWord");
//        String meaning = check.checkAnswer("meaning", "ci", "welshWord");
//        String englishNoun = check.checkAnswer("englishWord", "ci", "welshWord");
//        System.out.println("gender of ci = " + gender);
//        System.out.println("meaning of ci = " + meaning);
//        System.out.println("english word for ci = " + englishNoun);
    }
}
