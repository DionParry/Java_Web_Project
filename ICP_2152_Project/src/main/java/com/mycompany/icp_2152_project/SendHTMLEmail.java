/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;
// File Name SendHTMLEmail.java

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
//import javax.activation.*;

/**
 *
 * @author Dion
 */
public class SendHTMLEmail {

    /**
     *
     * @param args
     */
    public static void main(String args[]) {
//        Scanner sc = new Scanner(System.in);
//        System.out.print("gmail username: ");
//        String username = sc.next();
//        System.out.print("gmail password: ");
//        String password = sc.next();
//        System.out.print("destination email address: ");
//        String to = sc.next();
//        System.out.print("subject: ");
//        String subject = sc.next();
//        System.out.print("email body: ");
//        String email_body = sc.next();
        String username = "email";
        String password = "password";
        String to = "email reciver";
        String subject = "java";
        String email_body = "hi sexy";
        SendHTMLEmail test = new SendHTMLEmail();
        test.doSendMail(username, password, to, subject, email_body);
//        sc.close();

    }

    // sends mail

    /**
     *
     * @param username
     * @param password
     * @param to
     * @param subject
     * @param email_body
     */
    public void doSendMail(final String username, final String password, String to, String subject, String email_body) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp-relay.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(email_body);
            Transport.send(message);
            System.out.println("message sent");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
