/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.icp_2152_project;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jorda
 */
public class ResultsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    int FinalScore = 0;
            String userAnswerGM = "";

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResultsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            //out.println("<h1>Servlet ResultsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    QueryChecker check = new QueryChecker();
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String userAnswer = "";
        FinalScore = 0;
        
        userAnswer = request.getParameter("answer1");
        CheckAnswer(TakeTestServlet.Q1, "gender", "welshWord", userAnswer);
               
        userAnswer = request.getParameter("answer2");
        CheckAnswer(TakeTestServlet.Q2, "gender", "welshWord", userAnswer);
        
        userAnswer = request.getParameter("answer3");
        CheckAnswer(TakeTestServlet.Q3, "gender", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer4");
        CheckAnswer(TakeTestServlet.Q4, "gender", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer5");
        CheckAnswer(TakeTestServlet.Q5, "gender", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer16");
        CheckAnswer(TakeTestServlet.Q16, "gender", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer17");
        CheckAnswer(TakeTestServlet.Q17, "gender", "welshWord", userAnswer);
//        
        //Question 6-10
        userAnswer = request.getParameter("answer6");
        CheckAnswer(TakeTestServlet.Q6, "meaning", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer7");
        CheckAnswer(TakeTestServlet.Q7, "meaning", "welshWord", userAnswer);
//      
        userAnswer = request.getParameter("answer8");
        CheckAnswer(TakeTestServlet.Q8, "meaning", "welshWord", userAnswer);
//        
        userAnswer = request.getParameter("answer9");
        CheckAnswer(TakeTestServlet.Q9, "meaning", "welshWord", userAnswer);
        
        userAnswer = request.getParameter("answer10");
        CheckAnswer(TakeTestServlet.Q10, "meaning", "welshWord", userAnswer);
        
        userAnswer = request.getParameter("answer18");
        CheckAnswer(TakeTestServlet.Q18, "meaning", "welshWord", userAnswer);
        
        userAnswer = request.getParameter("answer19");
        CheckAnswer(TakeTestServlet.Q19, "meaning", "welshWord", userAnswer);
        
//        //Question 11-15

//        
     
        userAnswerGM = request.getParameter("gm1");
        CheckAnswerV2(TakeTestServlet.Q11, "welshWord", "englishWord", userAnswerGM,request.getParameter("answer11"));
        
        userAnswer = request.getParameter("answer12");
        userAnswerGM = request.getParameter("gm2");
        CheckAnswerV2(TakeTestServlet.Q12, "welshWord", "englishWord", userAnswerGM, userAnswer);
      
        userAnswer = request.getParameter("answer13");
        userAnswerGM = request.getParameter("gm3");
        CheckAnswerV2(TakeTestServlet.Q13, "welshWord", "englishWord", userAnswerGM,userAnswer);
        
        userAnswer = request.getParameter("answer14");
        userAnswerGM = request.getParameter("gm4");
        CheckAnswerV2(TakeTestServlet.Q14, "welshWord", "englishWord", userAnswerGM,userAnswer);
        
        userAnswer = request.getParameter("answer15");
        userAnswerGM = request.getParameter("gm5");
        CheckAnswerV2(TakeTestServlet.Q15, "welshWord", "englishWord",userAnswerGM,userAnswer);
       
        userAnswer = request.getParameter("answer20");
        userAnswerGM = request.getParameter("gm6");
        CheckAnswerV2(TakeTestServlet.Q20, "welshWord", "englishWord",userAnswerGM,userAnswer);
//        

        /*
        out.print("<h1> Final Score: " + FinalScore + "/20  </h1>");    
        //set values of their results into the results table
        out.print("<h1> Final Score: " + username +  "/20  </h1>");   */ 
        HttpSession session = request.getSession();
        String username =  (String) session.getAttribute("username");
        check.storeResults(username,FinalScore);
        response.sendRedirect("TestHistory.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     *
     * @param qNumber
     * @param column1
     * @param column2
     * @param ans
     */
    public void CheckAnswer(String qNumber, String column1, String column2, String ans) {
        String question = qNumber;
        String correctAnswer1 = check.checkAnswer(column1, question, column2);
        String userAnswer1 = ans;
        answerCorrectness(userAnswer1, correctAnswer1);
    }
    
    /**
     *
     * @param qNumber
     * @param column1
     * @param column2
     * @param genderMarker
     * @param ans
     */
    public void CheckAnswerV2(String qNumber, String column1, String column2, String genderMarker, String ans) {
        String question = qNumber;
        
        //get the value of the text
        String correctText = check.checkAnswer(column1, question, column2);
        String userAnswer1 = ans;
        
        //get the value for the gender marker
        String correctGM = check.checkAnswer("gender", question, column2);
        String userAnswer2 = genderMarker;
        
        if(correctText.equals(userAnswer1) && correctGM.equals(userAnswer2)) {
            FinalScore++;
        }
        
    }
    
    /**
     *
     * @param userAnswer
     * @param actualAnswer
     */
    public void answerCorrectness(String userAnswer, String actualAnswer) {
        if (userAnswer.equals(actualAnswer)) {
            FinalScore++;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}