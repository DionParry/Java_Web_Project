/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Dion
 * Created: 23-Mar-2017
 */

CREATE TABLE IF NOT EXISTS words
(
    wordID int auto_increment,
    welshWord VARCHAR(20) NOT NULL,
    englishWord VARCHAR(20) NOT NULL,
    gender CHAR(1) NOT NULL,
    meaning VARCHAR(255) NOT NULL,
    PRIMARY KEY (wordID) 
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS student
(
    username VARCHAR(30) NOT NULL,
    password VARCHAR(255) NOT NULL,
    dateOfBirth YEAR,
    email VARCHAR(30),
    PRIMARY KEY (username)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS instructor 
(
    instructNo INT auto_increment, 
    username VARCHAR(30) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(30),
    Primary Key(instructNo)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS admin
(
    adminNo int auto_increment,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(255) NOT NULL,
    Primary Key(adminNo)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS results
(
    username VARCHAR(30) NOT NULL,
    testNo INT auto_increment, 
    score INT (2),
    completionDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    primary key (testNo),
    foreign key (username) references student(username) ON UPDATE CASCADE
)ENGINE=InnoDB;